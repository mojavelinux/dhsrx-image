FROM linuxserver/libreoffice

MAINTAINER Dan Allen "dan@opendevise.com"

RUN apk --no-cache add build-base py3-pip ruby ruby-bundler ruby-dev tesseract-ocr tidyhtml && pip install unoconv

WORKDIR /docs

CMD ["/bin/bash"]
